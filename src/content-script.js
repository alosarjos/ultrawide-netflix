var initialized;

if (!initialized) {
    var marginLeft = (window.screen.width * 1.34 - window.screen.width) / 2;
    var marginTop = (window.screen.height * 1.34 - window.screen.height) / 2;
    var enabled = false;
    var zoomedCss =
        'position: relative; width: 134%; height: 134%; margin-left:-' +
        marginLeft +
        'px; margin-top:-' +
        marginTop +
        'px;';
    var mixCss = 'position: absolute; height: 112%; -moz-transform: scaleX(1.19);';
    var stretchedCss = 'object-fit: fill;';
    var normalCss = 'position: relative; width: 100%; height: 100%;';
    initialized = true;
}

browser.runtime.onMessage.addListener((request) => {
    let modCss;
    switch (request.mode) {
        case 'toggle-zoom':
            modCss = zoomedCss;
            break;
        case 'toggle-stretch':
            modCss = stretchedCss;
            break;
        case 'toggle-mix':
            modCss = mixCss;
            break;
        default:
            break;
    }

    if (!enabled) {
        if (document.getElementById('netflix-player') !== null) {
            document.querySelector('#netflix-player > div.player-video-wrapper > div > video').style.cssText = modCss;
            enabled = true;
        } else if (document.getElementsByClassName('VideoContainer').length > 0) {
            document.querySelector('.VideoContainer > div > video').style.cssText = modCss;
            enabled = true;
        } else {
            throw 'Error: Video Player not detected';
        }
    } else {
        if (document.getElementById('netflix-player') !== null) {
            document.querySelector(
                '#netflix-player > div.player-video-wrapper > div > video'
            ).style.cssText = normalCss;
            enabled = false;
        } else if (document.getElementsByClassName('VideoContainer').length > 0) {
            document.querySelector('.VideoContainer > div > video').style.cssText = normalCss;
            enabled = false;
        } else {
            throw 'Error: Video Player not detected';
        }
    }
    return Promise.resolve();
});
